# CSS themes for markdown documents

## Download this themepack

To download this themepack, you can run the following command:

```
git clone https://gitea.com/chopin42/css-themes
chmod +x style
sudo cp style /usr/bin/style
```

## How to style up your docs

1. Export the markdown file into HTML format
2. Run the command `style`. Then type the path to the exported file and the theme you want to have.
3. Open the link in your browser and export it into PDF using the option `Print to file` in your *print dialog*.