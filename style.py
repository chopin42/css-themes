import os
#from shutil import copy
from ntpath import basename

# get the file
filename = input("What is the HTML file's path? ")
f = open(filename).read()
print(f"Content of '{filename}' loaded.")

# get the theme
print("=== PLEASE CHOOSE A THEME IN THE LIST BELOW ===")
for theme in os.listdir("themes"):
    if theme.endswith(".css"):
        print(theme)
theme = input("What theme do you want for your document? ")

# apply the theme
head = """<head>
<link rel="stylesheet" href="themes/%s">"""

f = f.replace("<head>", head % theme)
print("Styling applied.")

# create the file
open(basename(filename), "w").write(f)

# Generate the link
print("You can now access the file in your browser using the following link:")
path = os.path.abspath(basename(filename))
print(f"file://{path}")
